<?php
//namespace says you belong inside this folder unless you get specific order
namespace App\Http\Controllers;

use App\Post;

class PagesController extends Controller {
// get index because you're wanting to get the homepage of the site you also add all of the other pages that need to be returned
	public function getIndex() {#
		# Posts will order by the most recent 4 items in the blog post
		$posts = Post::orderBy('created_at', 'desc')->limit(4)->get();
		return view('pages.welcome')->withPosts($posts);
		# process variable data or params
		# talk to the model
		# recieve from the model
		# compile or process data from the model if needed
		# pass that data to the correct view
	}

	public function getAbout() {
		// created two veriables first and last they hold my name's data
		# that is then taken by variable full which is equal to first and last putting to strings together

		$first = 'Joel';
		$last = 'Granite';

		$fullname = $first . " " . $last;
		$email = 'Joel.granite@sky.com';
		$data = [];
		$data['email'] = $email;
		$data['fullname'] = $fullname;
		return view('pages.about')->withData($data);

		//->withFullname($fullname)->withEmail($email); -- old way of doing it 
		//creating an array called data then add the email is equal to $email fullname etc, this is linked into the about.blade.php file adding the {{ $data['fullname'] }}  <--- this then says pass the full name viriable inside of the data array 

		// render the pages view about, with a viriable called fullname, and set fullname equal to fullname 
		// all above is passed to the view the with method takes two arguments 
	}

	public function getContact() {
		return view('pages.contact');
	}

	//public function postContact() {

	//}

}

// needs to be pages/welcome or pages.welcome etc if your pages are inside of a folder