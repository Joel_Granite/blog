<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Post;
use Session;

/*anything in blue you want to add it to the list above */

class PostController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // create a variable and store all the blog posts in it from the database
        // post all will take all the blog posts from the database and paginate them
        $posts = Post::orderBy('id', 'desc')->paginate(10);

        // return a view and pass it in the above variable 
        return view('posts.index')->withPosts($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        // validation rules are what you want it to do
        // adding to rules together you use |
        $this->validate($request, array(
                'title' => 'required|max:255', 
                'slug' => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
                'body' => 'required'
            ));

        // store in database
        $post = new Post;

        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->body = $request->body;

        $post->save();

        Session::flash('success', 'The blog post was successfully saved');

        // redirect to another page
        // after redirected the route is posts show page
        // brings you to the dynamic post that has just been made
        return redirect()->route('posts.show', $post->id);
    }

    /**
     * Display the specified resource.
     * render the view pass the viriable called post and set the virable and the view equal to our posts which should be matching the database
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->withPost($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // find the post in the database and save as a variable
        $post = Post::find($id);
        // return the view and pass in the variable we previouly created
        return view('posts.edit')->withPost($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // it will accept information from the request from the form field data it will accept all of that take it validate it and store it in the database !-->
        $post = Post::find($id);
        if ($request->input('slug') == $post->slug) {
        $this->validate($request, array(
                'title' => 'required|max:255',
                'body' => 'required'
            ));
        } else {
        // Validate the date       
        $this->validate($request, array(
                'title' => 'required|max:255',
                'slug' => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
                'body' => 'required'
            ));
    }

        // save the data to the database
        $post = Post::find($id);

        // input identify something from the post input that was passed in !-->
        $post->title = $request->input('title');
        $post->slug = $request->input('slug');
        $post->body = $request->input('body');
        // this will now save and change the updated timestamp 
        $post->save();
        // set flash data with success message 
        Session::flash('success', 'This post was successfully saved.');
        // redirect with the flash data to posts.show 
        return redirect()->route('posts.show', $post->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        $post->delete();

        Session::flash('success', 'The post was successfully deleted');
        return redirect()->route('posts.index');
    }
}
