<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
}

// this pulls every post in DB
//Post::all()

// this would pull that specific post from the DB
//Post::where('title' => 'My First Title')->get()