<!DOCTYPE html>
<html lang="en">
  <head>
    @include('partials._head') {{-- Include Partials so seperate the files, this takes all the head content from the head blade file --}}
  </head>

  <body>
    
    @include('partials._nav') {{-- Include Partials so seperate the files, this takes the nav content from the nav blade file --}}

    
  
      <div class="container">
        <!-- blade starts with an  this is to let the html know that it is now working with blade !-->
        <!-- this container will be filled with dynamic content from the this is now a layout !-->
    
        {{-- This is how you comment in yield --}}
    
        @include('partials._messages')

        @yield('content')

        @include('partials._footer')

      </div> <!-- end of container !-->

        @include('partials._javascript')

        @yield('scripts')
  </body>
</html>