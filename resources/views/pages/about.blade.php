@extends('layouts.app')  <!-- extends main is where this whole section content will be pulled / linked too e.g. the main.blade yield section !-->

@section('title', '| About')

@section('content')

      <div class="row">
        <div class="col-md-12">
            <h1>About me</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel nisi sit amet dolor ultricies faucibus. Duis eleifend laoreet odio sit amet vehicula. Cras pretium, elit nec cursus suscipit, dui lorem eleifend felis, a porta nulla neque interdum sem. Proin id hendrerit lectus. Curabitur condimentum semper dignissim. Donec non massa sed magna tincidunt porta ut at est. </p>
        </div>    
      </div>

@endsection


{{-- @section('sidebar')

@endsection --}}