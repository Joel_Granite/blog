@extends('layouts.app') <!-- this is linking to the main.blade file saying with the extends if it was inside of a folder you would use e.g. layouts.main !-->

{{-- @section('stylesheets')
  <link rel="stylesheet" type="text/css" href="styles">
@endsection this is used if you need to have a unique stylesheet that wouls only be present on one page rather than them all etc this is used in main blade yeild --}} 

@section('title', '| Homepage')

@section('content') <!-- everything inside of this section('content') is content until the endsection - this is then used in the main.blade container yeild content section !-->
  <!-- row is created to make element full width, col md is a meduim width coloum 12 full width !-->
    <div class="row">
      <div class="col-md-12">
        <div class="jumbotron">
          <h1>Welcome to the blog of an idiot</h1>
          <p class="lead">Joel Granite's blog of rambaling</p>
          <p><a class="btn btn-primary btn-lg" href="#" role="button">Popular Post</a></p>
        </div>
      </div>
    </div> <!-- end of header row !-->

    <div class="row">
      <div class="col-md-8">

        @foreach($posts as $post)

        <div class="post">
          <h3>{{ $post->title }}</h3>
          <!-- limits the post description to 300 characters on view !-->
          <p>{{ substr($post->body, 0, 300) }}{{ strlen($post->body) > 300 ? "..." : "" }}</p>
          <a href="{{ url('blog/'.$post->slug) }}" class="btn btn-primary">Read More</a>
        </div>

        <hr>

        @endforeach

      </div>
      <!-- the offset-1 bassically leaves a gap from the 8 coloum and the 3 coloum makes it upto the 12 grid coloum 8 add 3 add 1 space !-->
      <div class="col-md-3 col-md-offset-1">
        <h2>Sidebar</h2>
      </div>
    </div>

@endsection

{{-- @section('scripts')
  <script>
    confirm('I loaded up some JS');
  </script>
@endsection --}}