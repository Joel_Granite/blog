<!-- this will check the flash success from the postcontroller file if true then the success is displayed !-->
@if (Session::has('success'))

	<div class="alert alert-success" role="alert">
		<strong>Success:</strong> {{ Session::get('success') }}
	</div>

@endif

@if (count($errors) > 0)

	<div class="alert alert-danger" role="alert">
		<strong>Errors:</strong>
		<ul>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
		</ul>
	</div>

@endif

<!-- this will alert errors from a created loop if a requirement is not met the page will display a list of possible errors that caused it upon submition !-->