<!-- THIS FILE IS FOR THE MAIN NAV OF THE BLOG !-->

  <!-- mobile hamburger code !-->
  <!-- default bootstrap navbar !-->
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Laravel Blog</a>
      </div>

      <!-- top nav !-->
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li class="active"><a href="/">Home</a></li>
          <li><a href="/blog">Blog</a></li>
          <li><a href="/about">About</a></li>
          <li><a href="/contact">Contact</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <!-- if the user is logged in then display the drop down nav, if not just display a button to login !-->
          @if (Auth::check())
          <li class="dropdown">
            <!-- this auth will say hello and pull the users ->name from the DB !-->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hello {{ Auth::user()->name }} <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="/posts">Posts</a></li>
              <li><a href="/login">Login</a></li>
              <li><a href="/register">Register</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="{{ route('logout') }}">Logout</a></li>
            </ul>
          </li>
          
          @else

            <a href="{{ route('login') }}" class="btn btn-default">Login</a>

          @endif

        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>