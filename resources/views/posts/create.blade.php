@extends('main')

@section('title', '| Create new post')

@section('stylesheets')

	{!! Html::style('css/parsley.css') !!}

@endsection

@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h1>Create New Post</h1>
			<hr>
			<!-- this form is the same as a form tag but offers more Csrf protection secutrity feature !-->
			<!-- see php artisan route:list for detials !-->
			<!-- passing route parimiter going to post store in db, passing id the label always matching the column name of the db, the Title: is the title that will be above the form name on the browser, then create the form field create text and should submit it under the title field and that theyre linked, default value is null, second paramitor create array pass class to know what classes to give this, form control basicallt is the style !-->
			{!! Form::open(['route' => 'posts.store', 'data-parsley-validate' =>'']) !!}
  				{{ Form::label('title', 'Title:') }}
  				{{ Form::text('title', null, array('class' => 'form-control', 'required' => '', 'maxlength' =>'255')) }}

  				{{ Form::label('slug', 'Slug:') }}
  				{{ Form::text('slug', null, array('class' => 'form-control', 'required' => '', 'minlength' => '5', 'maxlength' => '255') ) }}

  				<!-- matching the column and the label for the db !-->

  				{{ Form::label('body', 'Post Body:') }}
  				{{ Form::textarea('body', null, array('class' => 'form-control', 'required' => '')) }}

  				<!-- submit is button then text the user sees, array give class btn is button, btn success is green, btn-lg is button large, btn-block is making it full width !--> 

  				{{ Form::submit('Create Post', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
			{!! Form::close() !!}
		</div>
	</div>

@endsection

@section('scripts')

	{!! Html::script('js/parsley.min.js') !!}

@endsection