@extends('main')

@section('title', '| All Posts')

@section('content')

	<div class="row">
		<div class="col-md-10">
			<h1>All Posts</h1>
		</div>
<!-- route will link to create a new blog post !-->
		<div class="col-md-2">
			<a href="{{ route('posts.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Create new post</a>	
		</div>
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- end of row !-->

	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<thead>
					<th>#</th>
					<th>Title</th>
					<th>Body</th>
					<th>Created at</th>
					<th></th>
				</thead>

				<tbody>
					<!-- loop that will continue to pull as long as there is another post avalible to show !-->
					<!-- this is setting the echo to display the data from the db these will be shown in the titles above !-->

					<!-- the substring will take only the first 50 characters and display them on the page !-->

					<!-- the string lengh will check to see if the body text is greater than 50 if so thats what the ? is used for then ... then if it isnt greater used by the : then do nothing !--> 
					@foreach ($posts as $post)

						<tr>
							<th>{{ $post->id }}</th>
							<td>{{ $post->title }}</td>
							<td>{{ substr($post->body, 0,50) }}{{ strlen($post->body) > 50 ? "..." : "" }}</td>
							<td>{{ date('M j, Y', strtotime($post->created_at)) }}</td>
							<td><a href="{{ route('posts.show', $post->id) }}" class="btn btn-default btn-sm">View</a> <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-default btn-sm">Edit</a></td>
						</tr>

					@endforeach

				</tbody>
			</table>
			<!-- this will add the pages buttons on the all posts page !-->
			<div class="text-center">
				{!! $posts->links(); !!}
			</div>
		</div>
	</div>

@endsection