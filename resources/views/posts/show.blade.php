@extends('main')

@section('title', '| View Post')

@section('content')

<!-- set the h1 equal to the value of the title colomn in the db !-->
<!-- set the p equal to the value of the body colomn in the db !-->
  <div class="row">
  	<div class="col-md-8">
		<h1>{{ $post->title }}</h1>
	
		<p class="lead">{{ $post->body }}</p>
	</div>

	<div class="col-md-4">
		<div class="well">

			<dl class="dl-horizontal">
			<label>Url Slug:</label>
				<!-- this post slug is how to pull directly from the db column !-->
				<p><a href="{{ route('blog.single', $post->slug) }}">{{ route('blog.single', $post->slug) }}</a></p>
			</dl>

			<dl class="dl-horizontal">
			<label>Created at:</label>
				<!-- this post created_at is how to pull directly from the db column !-->
				<p>{{ date('M j, Y h:ia', strtotime($post->created_at)) }}</p>
			</dl>
				<dt>Created at:</dt>
				<!-- this post created_at is how to pull directly from the db column !-->
				<p>{{ date('M j, Y h:ia', strtotime($post->created_at)) }}</p>
			</dl>

			<dl class="dl-horizontal">
				<label>Last updated:</label>
				<p>{{ date('M j, Y h:ia', strtotime($post->updated_at)) }}</p>
			</dl>

			<hr>
			<!-- this will take the html link route to post edit page, then the button name with the passed array for the post id that is shown on this page !-->
			<div class="row">
				<div class="col-sm-6">
				{!! Html::linkRoute('posts.edit', 'Edit', array($post->id), array('class' => 'btn btn-primary btn-block')) !!}
				
				</div>
			<!-- this will take the submit to delete, then the button name with the passed array for the post id that is shown on this page !-->
				<div class="col-sm-6">
				{!! Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'DELETE']) !!}

				{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

				{!! Form::close() !!}
				</div>
			</div>
	
			<div class="row">
				<div class="col-md-12">
					{{ Html::linkRoute('posts.index', '<< Back to all Posts', [], ['class' => 'btn btn-default btn-block btn-h1-spacing']) }}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

<!-- this will add a well that will contain the created at time and a last updated at and an edit blue button and a red delete button theyre inside of a small column 6  !--> 